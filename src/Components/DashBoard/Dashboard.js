import React from "react";
import { withRouter } from "react-router";
import { useHistory } from "react-router-dom";
import "./DashBoard.css";

function Dashboard (){
  const history = useHistory();
  const handeleLogout = (e) => {
    e.localStorage.clear();
    history.push({pathname:'/'});
  };
  return (
    <div>
      <h1>Welcome First Navigation </h1>
      {JSON.parse(localStorage.getItem("user")).Firstname}
      <br />
      <button
        type="submit"
        onClick={() => {
          handeleLogout();
        }}
        className="Logout"
      >
        Logout
      </button>
    </div>
  );
}

export default withRouter(Dashboard);
