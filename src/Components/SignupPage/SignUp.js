import React, { useEffect, useState } from "react";
import "./SignUp.css";
import { Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import SignUpSchema from "../../Validation/SignUpSchema";
import { withRouter } from "react-router";
import { useHistory } from "react-router-dom";

function SignUp() {
  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(SignUpSchema),
  });

  const [data, setData] = useState({});

  const onSubmit = (data) => {
    localStorage.setItem(
      "User",
      JSON.stringify({
        FirstName: "",
        LastName: "",
        Phone: "",
        Email: "",
        Password: "",
        Profesion: "",
      })
    );
    setData({
      FirstName: "",
      LastName: "",
      Phone: "",
      Email: "",
      Password: "",
      Profesion: "",
    });
  };

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("data"));

    if (user && user.FirstName) {
      setData({
        FirstName: user.FirstName,
        LastName: user.LastName,
        Phone: user.Phone,
        Email: user.Email,
        Password: user.Password,
        Profesion: user.Profesion,
      });
    }
  }, []);

  return (
    <div className="form-group">
      <h1>SignUp</h1>
      <form onSubmit={onSubmit}>
        <div>
          <label>FirstName</label>
          <input
            type="text"
            className="form-control"
            name="firstname"
            // onChange={(event) => handleChange(event)}
            placeholder="Enter Firstname"
          />
          {/*<p> {errors.Firstname?.message} </p>*/}
        </div>
        <div>
          <label>LastName</label>
          <input
            type="text"
            className="form-control"
            name="lastname"
            // onChange={(event) => handleChange(event)}

            placeholder="Enter Lastname"
          />
          {/* <p>{errors.lastname?.message}</p> */}
        </div>
        <div>
          <label>Phone</label>
          <input
            type="phone"
            className="form-control"
            name="phone"
            // onChange={(event) => handleChange(event)}

            placeholder="Enter Contact No"
          />
          {/* <p>{errors.phone?.message}</p> */}
        </div>
        <div>
          <label>Email</label>
          <input
            type="email"
            className="form-control"
            name="email"
            // onChange={(event) => handleChange(event)}

            placeholder="Enter Email"
          />
          {/* <p>{errors.email?.message}</p> */}
        </div>
        <div>
          <label>Password</label>
          <input
            type="password"
            className="form-control"
            name="password"
            // onChange={(event) => handleChange(event)}

            placeholder="Enter Password"
          />
          {/* <p>{errors.password?.message}</p> */}
        </div>
        <div>
          <label>Profesion</label>
          <Form.Control
            as="select"
            // onChange={(event) => handleChange(event)}
          >
            <option>Developer</option>
            <option>Artist</option>
            <option>Photographer</option>
            <option>Team Player</option>
            <option>Full Stack</option>
          </Form.Control>
        </div>
        <button type="submit" className="SignUp">
          SignUp
        </button>
      </form>
    </div>
  );
}

export default withRouter(SignUp);
