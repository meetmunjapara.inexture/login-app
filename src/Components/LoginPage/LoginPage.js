import React, { useEffect, useState } from "react";
import "./Login.css";
import { withRouter, Link, useHistory } from "react-router-dom";

function LoginPage() {
  
const history = useHistory();

const handleChange  = (data) =>{
    console.log(data);
}

  useEffect(() => {
    let isLoggedIn = localStorage.getItem("isLoggedIn");
    if (isLoggedIn) {
      history.push("/DashBoard");
    }
  }, []);

  const handleLogin = () => {
    const user = JSON.parse(localStorage.getItem("user"));
    if (
      user.Email == this.state.email &&
      user.Password == this.state.password
    ) {
      history.push("/Dashboard");
      localStorage.setItem("isLoggedIn", true);
    }
  };
  return (
    <div className="form-group">
      <h1>Login</h1>
      <div>
        <label>Email</label>
        <input
          type="text"
          onChange={(event) => handleChange(event)}
          placeholder="Enter Email"
          name="email"
          className="form-control"
        />
      </div>
      <div>
        <label>Password</label>
        <input
          type="password"
          placeholder="Enter Password"
          name="password"
          onChange={(event) => handleChange(event)}
          className="form-control"
        />
      </div>
      <button type="submit" onClick={handleLogin} className="loginButton">
        Login
      </button>

      <Link to="/">Sign Up</Link>
    </div>
  );
}

export default withRouter(LoginPage);
