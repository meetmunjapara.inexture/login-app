import React, { Component } from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Dashboard from "./Components/DashBoard/Dashboard";
import LoginPage from "./Components/LoginPage/LoginPage";
import SignUp from "./Components/SignupPage/SignUp";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route path="/SignUp">
              <SignUp />
            </Route>
            <Route path="/Login">
              <LoginPage />
            </Route>
            <Route path="/DashBoard">
              <Dashboard />
            </Route>
            <Route path="/">
              <Redirect to="/SignUp" />
            </Route>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
