import * as yup from "yup";

const validatePhone = (value) => {
  if (!value) {
    return "Field Required";
  } else {
    if (isNaN(value)) {
      return "Mobile number must write only digit not characters.";
    } else if (value.length !== 10) {
      return "Digit 10 Only";
    }
  }
  return null;
};
const validateEmail = (value) => {
  if (!value) {
    return true;
  }
  return value.trim().length > 0;
};
const validatePassword = (value) => {
  if (!value) {
    return "Field Required";
  }
  return null;
};
const SignUpSchema = yup.object().shape({
  firstname: yup
    .string()
    // .firstName("Please Enter Valid FirstName")
    .min(3)
    .max(10)
    .required("*Required"),
  lastname: yup
    .string()
    // .lastName("Please Enter Valid LastName")
    .min(3)
    .max(10)
    .required("*Required"),
  phone: yup
    .string()
    .test("Validate Phone", "Please Enter Valid Phone", validatePhone)
    // .phone("Please Enter Valid Phone.")
    .required("*Required"),
  email: yup
    .string()
    .test("Validate Email", "Please Enter Valid Email", validateEmail)
    // .email("Please Enter Valid Email.")
    .required("*Required"),
  password: yup
    .string()
    .test("Validate Password", "Please Enter Valid Password", validatePassword)
    .required("*Required")
    .matches(
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\S).{8,}$/i,
      "Password expresion that required one lower case letter,one upper case later , one digit ,6-13 length,no space"
    ),
});

export default SignUpSchema;